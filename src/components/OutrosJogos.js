import React, { Component } from 'react';
import { Text, StyleSheet, View, Image, Button, Linking } from 'react-native';

export default class OutrosJogos extends Component{
    render(){
        return(
            <View style={styles.bodyOutros}>
                <Text style={styles.txtTitulo}> Jokenpo (Pedra, Papel e Tesoura) </Text>
                <Image style={styles.imagem} source={require('../imgs/jokenpo.png')} />
                <Text style={styles.txtDescricao}> O jogo Jokenpo, também conhecido como "Pedra, Papel e Tesoura", é facil
                e o seu oponente é o proprio celular.</Text>
                <Button 
                onPress={()=> Linking.openURL('https://gitlab.com/michelvictor16/jokenpo')}
                title='Projeto no GitLab'
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bodyOutros:{
        flex:1,
        backgroundColor: '#61BD8C',
        padding: 20
    },
    imagem:{
        height: 200,
        width: 400
    },
   txtTitulo:{
       color: '#FFF',
       fontSize: 22
   },
   txtDescricao:{
    color: '#FFF',
    fontSize: 18,
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'justify'
   }
})