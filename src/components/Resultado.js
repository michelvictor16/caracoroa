import React, { Component } from 'react';
import { Image, View, StyleSheet, Button } from 'react-native';

export default class Resultado extends Component{
    constructor(props){
        super(props);

        this.state={resultado: '' };
    }

    componentWillMount(){
        const numAleatorio = Math.floor( Math.random() * 2);
        let caraCoroa = '';

        if(numAleatorio === 0){
            caraCoroa = 'cara';
        } else{
            caraCoroa = 'coroa';
        }

        this.setState({ resultado: caraCoroa});
    }

    render(){
        if(this.state.resultado ==='cara'){
            return(
                <View style={styles.resultado}>
                    <Image source={require('../imgs/moeda_cara.png')}/>
                    <View style={styles.botao}>
                        <Button 
                        onPress={()=> this.componentWillMount()}
                        title="Jogar novamente" 
                        />
                    </View>
                </View>
            );
        } else {
            return(
                <View style={styles.resultado}>
                    <Image source={require('../imgs/moeda_coroa.png')}/>
                    <View style={styles.botao}>
                        <Button 
                        onPress={()=> this.componentWillMount()}
                        title="Jogar novamente"/>
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    resultado:{
        flex: 1,
        backgroundColor: '#61BD8C',
        justifyContent: 'center',
        alignItems: 'center'
    },
    botao:{
        marginTop: 20
    }
});